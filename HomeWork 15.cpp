﻿
#include <iostream>

static int N;

void function(int N, int y)
{
    for (int i = y; i <= N; i += 2)
        std::cout << i << " ";
    std::cout << "\n";
}


int main()
{
    std::cout << "Enter N: ";
    std::cin >> N;
    

    std::cout << "Even numbers from 0 to N:\n";
    function(N, 2);

    std::cout << "Odd numbers from 0 to N:\n";
    function(N, 1);

}

